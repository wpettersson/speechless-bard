function toColour(col) {
  var out = col.toString(16);
  if (out.length == 1) {
    out = "0" + out;
  }
  return out;
}

function toTriple(r,g,b) {
  return "#" + toColour(r) + toColour(g) + toColour(b);
}


var stage = new Konva.Stage({
  container: 'bracelet',
  width: 800,
  height: 300
});
var layer = new Konva.Layer({
  id: 'layer'
});
var base = new Konva.Rect({
  x: 40,
  y: 40,
  height: 100,
  width: 600,
  fill: 'brown',
  name: 'top'
});

var diathree = new Konva.Rect({
  x: 100,
  y: 70,
  height: 30,
  width: 30,
  rotation: 45,
  fill: 'blue',
  name: 'dia bot'
});
var hex = new Konva.RegularPolygon({
  x: 180,
  y: 90,
  height: 40,
  width: 40,
  sides: 6,
  fill: 'blue',
  name: 'dia bot'
});
var tri = new Konva.RegularPolygon({
  x: 260,
  y: 95,
  height: 50,
  width: 50,
  sides: 3,
  fill: 'blue',
  name: 'dia bot'
});

var dia = new Konva.Rect({
  x: 400,
  y: 70,
  height: 30,
  width: 30,
  rotation: 45,
  fill: 'blue',
  name: 'dia bot'
});
var square = new Konva.Rect({
  x: 460,
  y: 70,
  height: 40,
  width: 40,
  fill: 'blue',
  name: 'square bot'
});
var diatwo = new Konva.Rect({
  x: 560,
  y: 70,
  height: 30,
  width: 30,
  rotation: 45,
  fill: 'blue',
  name: 'square bot'
});
layer.add(base);
layer.add(square);
layer.add(dia);
layer.add(diatwo);
layer.add(diathree);
layer.add(hex);
layer.add(tri);

stage.add(layer);
layer.draw();



function botClicked(element) {
  $(".swatch.bot").removeClass("selected");
  element.addClass("selected");
  stage.find(".bot").fill(element.css("background-color"));
  layer.draw();
}

function topClicked(element) {
  $(".swatch.top").removeClass("selected");
  element.addClass("selected");
  console.log(element.css("background-color"));
  stage.find(".top").fill(element.css("background-color"));
  layer.draw();
}

function makeBotColours() {
  var colours = [toTriple(202, 0, 0), toTriple(159, 197, 232),
    toTriple(241, 194, 50), toTriple(255, 153, 0), toTriple(8, 50, 89),
    toTriple(51, 116, 174), toTriple(39, 78, 19), toTriple(106, 168, 79),
    toTriple(217, 210, 233), toTriple(234, 209, 220), toTriple(204, 204, 204),
    toTriple(0, 0, 0), toTriple(191, 144, 0), toTriple(180, 95, 6),
    toTriple(178, 99, 99)
  ];
  colours.forEach(function(colour) {
    $("#colours-bot").append("<div style=\"background: " + colour + "\" class=\"swatch bot\"> </div>");
  });
  $(".swatch.bot").click(function() {
    botClicked($(this));
  });
}

function makeTopColours() {
  var colours = [toTriple(202, 142, 79), toTriple(0, 0, 0), toTriple(153, 153, 153), toTriple(61, 133, 198), toTriple(204, 0, 0), toTriple(182, 215, 168), toTriple(69, 129, 142), toTriple(180, 167, 214)];
  colours.forEach(function(colour) {
    $("#colours-top").append("<div style=\"background: " + colour + "\" class=\"swatch top\"> </div>");
  });
  $(".swatch.top").click(function() {
    topClicked($(this));
  });
}

$(document).ready(function() {
  makeTopColours();
  makeBotColours();
});
